<article id="post-<?php the_ID(); ?>" <?php post_class( ' post-details' ); ?>>
	<!-- Article content -->
    <?php
        $speaker_photo        = get_field('speaker_profile_photo');
        $speaker_designation  = get_field('speaker_designation');
        $speaker_description  = get_field('speaker_description');
        $speaker_comapny_name = get_field('speaker_comapny_name');
        $speaker_profession   = get_field('speaker_profession');
        $speaker_email        = get_field('speaker_email');
        $speaker_website      = get_field('speaker_website');
        $speaker_social       = get_field('speaker_social_media');
        $gallery              = get_field('galeria');

    ?>


    <div class="speaker-personal-details-wraper">
        <div class="row">
            <?php if ($speaker_photo != '') { ?>
            <div class="col-lg-4 col-md-5">
                <div class="speaker-avatar">
                    <?php echo wp_get_attachment_image($speaker_photo['id'], 'full', false, array(
                        'class'  => 'img-fluid',
                        'alt'    => get_the_title())); 
                    ?>
                </div><!-- .speaker-avatar end -->
            </div>
            <?php } ?>
            <div class="<?php echo esc_attr( $speaker_photo != '' ? "col-lg-8 col-md-7" : "col-lg-12" ) ?>">
                <div class="speaker-details-wraper">
                    <div class="xs-speaker-details">
                        <div class="speaker-title-intro-details">
                            <h2 class="xs-speaker-title"><?php echo esc_html(get_the_title())?></h2><!-- speaker title -->

                            <?php if($speaker_designation != '' || $speaker_profession != '') {?>
                            <ul class="speaker-details-meta list-unstyled">
                                <?php if($speaker_designation != '') {?>
                                <li>
                                    <span><?php echo esc_html($speaker_designation)?></span><!-- speaker designation -->
                                </li>
                                <?php } ?>

                                <?php if($speaker_profession != '') {?>
                                <li>
                                    <span><?php echo esc_html($speaker_profession)?></span><!-- speaker profession -->
                                </li>
                                <?php } ?>
                            </ul>
                            <?php } ?>

                            <?php if($speaker_email != '' || $speaker_website != '') {?>
                            <ul class="speaker-details-meta list-unstyled speaker-info">
                                <?php if($speaker_website != '') {?>
                                <li>
                                    <a href="<?php echo esc_url($speaker_website)?>"><i class="fas fa-link"></i><?php echo esc_html($speaker_website)?></a><!-- speaker website -->
                                </li>
                                <?php } ?>

                                <?php if($speaker_email != '') {?>
                                <li>
                                    <a href="mailto:<?php echo esc_attr($speaker_email)?>"><i class="far fa-envelope"></i><?php echo esc_html($speaker_email)?></a><!-- speaker email -->
                                </li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                        </div>

                        <?php if (count($speaker_social) > 0) { ?>
                        <ul class="spekaer-social-list">
                            <?php foreach($speaker_social as $social) {
                                if ($social['social_icon'] != '') {
                            ?>
                            <li>
                                <a href="<?php echo esc_url($social['website_link']); ?>" class="<?php echo esc_attr($social['social_icon']); ?>"></a>
                            </li>
                            <?php
                                }
                            }
                            ?>
                        </ul>
                        <?php } ?>
                    </div>
                    <?php if ($speaker_description !== "") { ?>
                    <span class="xs-hr-line"></span>
                    <p class="xs-speaker-description"><?php echo evenex_kses($speaker_description); ?></p>
                    <?php } ?>
                </div><!-- .speaker-details-wraper end -->
            </div>
        </div>
    </div>


    <div class="speaker--gallery">

        <ul class="speaker--gallery--images">
            <?php foreach($gallery as $index=>$image) {
                ?>
                <div class="speaker--gallery--images--image">
                    <img src="<?php echo $image["sizes"]["medium_large"]; ?>" alt="" onclick="openModal();currentSlide(<?php echo $index + 1; ?>)">
                </div>
                <?php
            }
            ?>
        </ul>
    </div>

</article>




<div class="gallery--lightbox">

  <style>

  .gallery--lightbox .row > .column {
    padding: 0 1rem;
  }

  .gallery--lightbox .row:after {
    content: "";
    display: table;
    clear: both;
  }


  /* The Modal (background) */
  .gallery--lightbox .lightbox-modal {
    display: none;
    position: fixed;
    z-index: 1;
    padding-top: 100px;
    left: 0;
    top: 2rem;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: black;
  }

  /* Modal Content */
  .gallery--lightbox .lightbox-modal-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    width: 90%;
    max-width: 1200px;
  }

  /* The Close Button */
  .gallery--lightbox .close {
    color: white;
    position: absolute;
    top: 9rem im !important;
    right: 2rem;
    font-size: 35px;
    font-weight: bold;
    opacity: 1;
  }

  .gallery--lightbox .close:hover,
  .gallery--lightbox .close:focus {
    color: #999;
    text-decoration: none;
    cursor: pointer;
  }

  .gallery--lightbox .lightbox-slide {
    display: none;
  }

  .gallery--lightbox .cursor {
    cursor: pointer;
  }

  /* Next & previous buttons */
  .gallery--lightbox .prev,
  .gallery--lightbox .next {
    cursor: pointer;
    position: absolute;
    top: 50%;
    width: auto;
    padding: 16px;
    margin-top: -50px;
    color: white !important;
    font-weight: bold;
    font-size: 20px;
    transition: 0.6s ease;
    border-radius: 0 3px 3px 0;
    user-select: none;
    -webkit-user-select: none;
    opacity: 1;
  }

  /* Position the "next button" to the right */
  .gallery--lightbox .next {
    right: 0;
    border-radius: 3px 0 0 3px;
  }

  /* On hover, add a black background color with a little bit see-through */
  .gallery--lightbox .prev:hover,
  .gallery--lightbox .next:hover {
    background-color: rgba(0, 0, 0, 0.8);
  }

  /* Number text (1/3 etc) */
  .gallery--lightbox .numbertext {
    color: #f2f2f2;
    font-size: 12px;
    padding: 8px 12px;
    position: absolute;
    top: 0;
  }

  img {
    margin-bottom: -4px;
  }

  .gallery--lightbox .caption-container {
    text-align: center;
    background-color: black;
    padding: 2px 16px;
    color: white;
  }

  .gallery--lightbox .demo {
    opacity: 0.6;
  }

  .gallery--lightbox .active,
  .gallery--lightbox .demo:hover {
    opacity: 1;
  }

  img.hover-shadow {
    transition: 0.3s;
  }

  .gallery--lightbox .hover-shadow:hover {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  }

  .gallery--lightbox img {
    width: 100% !important;
    height: 100% !important;
    object-fit: contain !important;
  }

  .speaker--gallery--lightbox--images--image--selector img {
    object-fit: cover !important;
  }


  .gallery--lightbox .lightbox-slide {
    height: calc(100vh - 19rem);
    margin-top: 1rem;
    margin-bottom: 1rem;
  }
  .gallery--lightbox,
  .gallery--lightbox * {
    background-color: #000 !important;
  }

  .speaker--gallery--lightbox--images--selectors {
    width: 100%;
    height: 7rem;
    display: flex;
    justify-content: flex-start;
    overflow: auto;
    flex-wrap: wrap;
    flex-direction: column;
    overflow-y: hidden;
  }

  .speaker--gallery--lightbox--images--image--selector {
    width: 7rem;
    height: 7rem;
    padding-bottom: 1rem;
  }
  .speaker--gallery--lightbox--images--image--selector img {
    height: 6rem !important;
    margin-bottom: 1rem;
  }
  </style>


  <div id="lightbox-modal" class="lightbox-modal">
      <span class="close cursor" onclick="closeModal()">&times;</span>
      <div class="lightbox-modal-content">
          <?php foreach($gallery as $index=>$image) {
              ?>
              <div class="speaker--gallery--lightbox--images--image lightbox-slide">
                  <div class="numbertext"><?php echo $index + 1; ?> / <?php echo count($gallery); ?></div>                
                  <img src="<?php echo $image["sizes"]["large"]; ?>" alt="" onclick="openModal();currentSlide(<?php echo $index + 1; ?>)" alt=""  style="width:100%">
              </div>
              <?php
          }
          ?>
      
      <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
      <a class="next" onclick="plusSlides(1)">&#10095;</a>

      <div class="caption-container">
        <p id="caption"></p>
      </div>

      <div class="speaker--gallery--lightbox--images--selectors">
      <?php foreach($gallery as $index=>$image) {
          ?>
          <div class="speaker--gallery--lightbox--images--image--selector">
              <img class="demo cursor" src="<?php echo $image["sizes"]["thumbnail"]; ?>" alt="" onclick="currentSlide(<?php echo $index + 1; ?>)" alt="">
          </div>
          <?php
      }
      ?>
      </div>


    </div>
  </div>

  <script>
  function openModal() {
    document.getElementById("lightbox-modal").style.display = "block";
  }

  function closeModal() {
    document.getElementById("lightbox-modal").style.display = "none";
  }

  var slideIndex = 1;
  showSlides(slideIndex);

  function plusSlides(n) {
    showSlides(slideIndex += n);
  }

  function currentSlide(n) {
    showSlides(slideIndex = n);
  }

  function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("lightbox-slide");
    var dots = document.getElementsByClassName("demo");
    var captionText = document.getElementById("caption");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " active";
    captionText.innerHTML = dots[slideIndex-1].alt;
  }
  </script>

</div>