<?php

if ( !defined( 'WP_DEBUG' ) ) {
	die( 'Direct access forbidden.' );
}

function evenex_theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

add_action( 'wp_enqueue_scripts', 'evenex_theme_enqueue_styles' );


function gobmx_scripts() {
//	wp_register_script('modernizer', "https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js", null, null, true );
//	wp_enqueue_script('modernizer');
	
	wp_register_script('gobmx', get_stylesheet_directory_uri() . '/js/gobmx.js', ["jquery"], null, true );
	wp_enqueue_script('gobmx');
	
	wp_enqueue_style( 
      'style', 
      get_stylesheet_directory_uri() . '/style.css' 
    );
	
}

add_action( 'elementor/frontend/before_enqueue_scripts', 'gobmx_scripts', 20, 1);


add_filter('show_admin_bar', '__return_false');


function clean_src( $src, $handle ) {
    if (preg_match('_\d[^\?/]*\?_', $src))
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'clean_src', 10, 2 );
add_filter( 'script_loader_src', 'clean_src', 10, 2 );

function speaker_images() {

	add_theme_support('post-thumbnails');
		
	add_post_type_support( 'xs-speaker', 'thumbnail' );
	add_post_type_support( 'xs-speaker', 'excerpt' );

}
add_action( 'init', 'speaker_images' );
    